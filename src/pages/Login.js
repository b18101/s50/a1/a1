import {useState, useEffect, useContext} from 'react';
import { Form, Button } from "react-bootstrap";
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login(props){
    console.log(props);

    const {user, setUser} = useContext(UserContext);
    console.log(user)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function loginUser(e){

        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: 'Login Successful',
                    icon: 'success',
                    text: 'Welcome to Booking App of 182'
                })
            } else {
                
                Swal.fire({
                    title: 'Authentication Failed',
                    icon: 'error',
                    text: 'check your credentials'
                })
            }
        })

        // localStorage.setItem('email', email); replaced by the if statement above

        // setUser({
        //     email: localStorage.getItem('email'),
        // });

        setEmail('');
        setPassword('');

        // alert(`${email} has been verified! Welcome back!`);
    }

    const retrieveUserDetails = (token) => {

        fetch('http://localhost:4000/users/getUserDetails', {
            // method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


    useEffect(() => {
        if((email !== '' && password !== '')){

            setIsActive(true);

        } else {

            setIsActive(false);

        }
    }, [email, password]);

    return (

        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <>
        <h1>Login Here:</h1>
        <Form onSubmit={e => loginUser(e)}>
            <Form.Group controlId="loginEmail">
                <Form.Label>Email Address:</Form.Label>
                    <Form.Control
                        type= "email"
                        placeholder= "Please input your email here"
                        required
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
            </Form.Group>

            <Form.Group controlId="loginPassword">
                <Form.Label>Password:</Form.Label>
                <Form.Control
                    type= "password"
                    placeholder= "Please input your password here"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group>

            { isActive ?
                <Button variant="success" type="submit" id="loginBtn" className="mt-3 mb-5">
                    Submit
                </Button>

                :

                <Button variant="danger" type="submit" id="loginBtn" className="mt-3 mb-5" disabled>
                    Submit
                </Button>
            }
        </Form>
        </>
    )
}